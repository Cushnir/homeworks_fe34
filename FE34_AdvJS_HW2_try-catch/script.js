// Массив книг
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

// вставляем элемент ul в div с id="root"
const container = document.getElementById("root");
const booksList = document.createElement("ul");
container.append(booksList);

// проходим по объектам массима
books.forEach((elem) => {
    try {
        // генерируем ошибку по условию. На основе встроенного класса SyntaxError 
        // создаем объект ошибки, и передаем в него сообщение
        if (!elem.name) {
            throw new SyntaxError(`У книги нет свойства "name"`);
        }
        if (!elem.author) {
            throw new SyntaxError(`У книги "${elem.name}" нет свойства "author"`);
        }
        if (!elem.price) {
            throw new SyntaxError(`У книги "${elem.name}" нет свойства "price"`);
        }
        // патаемся выполнить, если "проскочили" исключения
        // выводим html-код в конец списка ul
        booksList.insertAdjacentHTML('beforeend',
            `<li> book: "${elem.name}"  /  autor: ${elem.author}  /  price: ${elem.price} </li>`);

    } catch (err) { // обрабатываем ошибку, выводим в консоль пользовательское сообщение
        console.log(err.message);
    }
});