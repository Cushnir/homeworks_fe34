let divRoot = document.getElementById("root");

divRoot.addEventListener("click", btnDelClick);

function btnDelClick(event) {
    if (event.target.classList.contains("btn-delete")) {
        requestPostDelete(event.target.dataset.btn);
        event.target.closest(".card").remove();
        console.log("Post #" + event.target.dataset.btn + " deleted!");
    }
}

class Card {
    constructor(params) {
        this.typeOfElement = params.typeOfElement,
            this.parentOfElement = params.parentOfElement,
            this.classOfElement = params.classOfElement,
            this.datasetVolume = params.datasetVolume
    }
    insertNewElement() {
        let newElem = document.createElement(this.typeOfElement);
        newElem.className = this.classOfElement;
        this.parentOfElement.insertAdjacentElement("beforeend", newElem);
        this.newElem = newElem;
    }
    insertHTML(strHtml) {
        this.newElem.innerHTML = strHtml;
    }
}

//создаем экземпляр card класса Card
const card = new Card({
    typeOfElement: "div",
    parentOfElement: divRoot,
    classOfElement: "card",
    dataset: "data-id"
})

// ==== З А П Р О С ========================================
let urlUsers = "https://ajax.test-danit.com/api/json/users";
//let urlPosts = "https://ajax.test-danit.com/api/json/posts";

let requstUsers = fetch(urlUsers, { method: 'GET' });

requstUsers.then(users => users.json())
    .then(users => {
        users.forEach(userElem => {
            fetch(`https://ajax.test-danit.com/api/json/posts?userId=${userElem.id}`)
                .then(postsData => postsData.json())
                .then(postsData => {
                    postsData.forEach(post => {
                        card.insertNewElement();
                        card.insertHTML(`<div class="card__header">
                                            <span class="header-user-name">${userElem.name}</span>
                                            <span class="header-user-nik">@${userElem.username}</span>
                                            <button class="btn-delete" data-btn="${post.id}">delete post</button>
                                            <span class="header-user-email">${userElem.email}</span>
                                        </div>
                                        <div class="card__post-wrapper">
                                            <h3 class="card__post-title">${post.title}</h3><hr/>
                                            <p class="card__post-text">${post.body}</p>
                                            <span class="temp">post-Id: ${post.id} </span>
                                            <span class="temp">user-Id: ${userElem.id}</span>
                                        </div>`);
                    })
                });
        })
    })


function requestPostDelete(postId) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE'
    }).then(response => console.log("Respon 'status': " + response.status + " 'ok': " + response.ok));
}