//TASK 1 ===== DONE
console.log('TASK #1 ---------------------------------------------------------------------------------');
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let allClients = [...new Set([...clients1, ...clients2])];
console.log(allClients);

//TASK 2 ===== DONE
console.log('\n');
console.log('TASK #2 ---------------------------------------------------------------------------------');
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

 //variant 01 begin ------------------------------------------
 console.log('arrey of characters');
console.log(characters); // выводим "старый" массив

const charactersShortInfo = characters.map(item => {
    let { gender, status, ...rest } = item;  // делаем деструктуризацию объектов в элементах массива разделяем
                                             // свойства gender, status, остальные кладем в "массив rest"
    return rest; // возвращаем только "массив rest"
})
console.log('arrey of charactersShortInfo');
console.log(charactersShortInfo); // выводим "новый" массив

 //variant 01 end ---------------------------------------------


 //variant 02 begin -----------------------------

// console.log(characters); // выводим "старый" массив

// let charactersShortInfo = [...characters];  // делаем НЕГЛУБОКУЮ копию массива. Теперь это "новый" массив. 
//                                             // Но ссылки на объекты вэлементах "нового" массива ссылаются на 
//                                             //объекты в элементах "старого" массива.
        
// charactersShortInfo.forEach((item, index) => {  // проходим по "новому" массиву
//     charactersShortInfo[index] = { ...item };   // 1 - поочередно клонируем объекты из "старого" массива в элементы "нового".
//                                                 //     Теперь это клоны объектов, и они не ссылаются на "старые" объекты
//     delete charactersShortInfo[index].gender;   // 2 - удаляем ненужные свойства из "новых" объектов, не затрагивая "старые"
//     delete charactersShortInfo[index].status;
// });

// console.log(charactersShortInfo); // выводим "новый" массив

// variant 02 end -----------------------------


//TASK 3 ===== DONE
console.log('\n');
console.log('TASK #3 ---------------------------------------------------------------------------------');
const user1 = {
    name: "John",
    years: 30
};

let {name, years: age, isAdmin = false} = user1;

console.log('name: ' + name);
console.log('age: ' + age);
console.log('isAdmin: ' + isAdmin);

//TASK 4 ===== DONE
console.log('\n');
console.log('TASK #4 ---------------------------------------------------------------------------------');
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);


//TASK 5 ===== DONE
console.log('\n');
console.log('TASK #5 ---------------------------------------------------------------------------------');
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const allBooks = [...books, bookToAdd];
console.log(allBooks);
console.log(books);

//TASK 6 ===== DONE
// Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный
// объект (должен быть создан новый объект, который будет включать все необходимые свойства).
// Выведите новосозданный объект в консоль.
console.log('\n');
console.log('TASK #6 ---------------------------------------------------------------------------------');
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const employeeExstended = ({...employee, age: 'default', salary: 'default'});
console.log(employeeExstended);
console.log(employee);

//TASK 7 ===== DONE
console.log('\n');
console.log('TASK #7 ---------------------------------------------------------------------------------');
console.log('used alert!');
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // выводит 'value'
alert(showValue());  // выводит 'showValue'


