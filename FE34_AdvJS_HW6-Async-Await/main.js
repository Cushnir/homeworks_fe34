const root = document.getElementById('root');

const buttonGetByIp = document.createElement('button');
buttonGetByIp.innerText = 'Вычислить по IP';
root.insertAdjacentElement('afterbegin', buttonGetByIp);

const placeInfo = document.createElement('p');
root.insertAdjacentElement('beforeend', placeInfo);

document.addEventListener("click", (event) => {
    if (event.target === buttonGetByIp) getIpAndLocationInfo();
});

let url = 'https://api.ipify.org/?format=json';

async function getIpAndLocationInfo() {
    const resivedIp = await fetch(url).then(data => data.json());
    const resivedPlaceInfo = await fetch(`http://ip-api.com/json/${resivedIp.ip}?fields=country,city`);
    placeInfo.innerText = `страна: ${resivedPlaceInfo.country}, город: ${resivedPlaceInfo.city}`;
}

