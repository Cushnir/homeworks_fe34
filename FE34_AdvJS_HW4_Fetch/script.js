let divRoot = document.getElementById('root');

let url = "https://ajax.test-danit.com/api/swapi/films";

function createFilm(episode_id, title, opening_crawl) {
    let filmHeader = document.createElement('div');
    filmHeader.setAttribute('data-film', episode_id);
    filmHeader.style.backgroundColor = '#eee';

    filmHeader.innerHTML = `<span>episode#${episode_id}</span><h2>"${title}"</h2><p>${opening_crawl}</p><ul></ul>`;
    divRoot.insertAdjacentElement("beforeend", filmHeader);
}

function addCharacterListToFilm(characterName, index) {
    let filmInfo = document.querySelectorAll('[data-film]');
    filmInfo.forEach(elem => {
        if (+elem.dataset.film === index + 1) {
            elem.lastChild.insertAdjacentHTML('beforeend', `<li>${characterName}</li>`);
        }
    });
}

function getCharacterName(url, index) {
    let responceCharacter = fetch(url);

    responceCharacter
        .then(data => {
            return data.json();
        }).then(character => {
            addCharacterListToFilm(character.name, index);
        });
}

function sortFilmsByEpisode(field) {
    return (a, b) => (a[field] > b[field] ? 1 : -1);
}

let responceFilmInfo = fetch(url);

responceFilmInfo
    .then(content => {
        return content.json();
    }).then(filmInfo => {
        filmInfo.sort(sortFilmsByEpisode('episodeId'));
        filmInfo.forEach(element => {
            createFilm(element.episodeId, element.name, element.openingCrawl);
        });
        return filmInfo;
    }).then(filmInfo => {
        filmInfo.forEach((element, index) => {
            element.characters.forEach(elem => {
                getCharacterName(elem, index);
            })
        })
    }).catch(error=>{
        console.log(error);
    })