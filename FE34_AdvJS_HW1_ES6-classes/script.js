class Employee {
    constructor(name, age, salary) { // конструктор класса. Позволяет при создании
      this._name = name;             // экземпляра заполнить поля.
      this._age = age;
      this._salary = salary;
    }
    // геттеры и сеттеры для свойств
    get name() { 
        return this._name; 
    }

    set name(newName) {
      if (typeof newName === "string") {
        this._name = newName;
      }
    }

    get age() {
      return this._age;
    }

    set age(newAge) {
      if (!newAge.isNaN) {
        this._age = newAge;
      }
    }

    get salary() {
      return this._salary;
    }

    set salary(newSalary) {
      if (!newSalary.isNaN) {
        this._salary = newSalary;
      }
    }
  }
  // Создаем расширенный от Employee класс Programmer
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary); // эти свойства взяты от родительского класса
      this._lang = lang;       // это новое для класса Programmer
    }
    // геттеры и сеттеры для нового свойства
    get salary() {
      return this._salary * 3;
    }

    get lang() {
      return this._lang;
    }

    set lang (newLang) {
      if (typeof newLang === "string") {
        this._lang = newLang;
      }
    }
  }

  //Создаем экземпляры класса Programmer
  let programmerOne = new Programmer('aName', 10, 101, 'England');
  let programmerTwo = new Programmer('bName', 20, 202, 'Ukraine');
  let programmerThree = new Programmer('cName', 30, 303, 'Chineze');
  // Выводим в консоль объекты и умноженное на 3 свойство 'salary'
  console.log(programmerOne);
  console.log('programmerOne salary:' + programmerOne.salary);
  console.log(programmerTwo);
  console.log('programmerTwo salary:' + programmerTwo.salary);
  console.log(programmerThree);
  console.log('programmerThree salary:' + programmerThree.salary);